﻿using System;
using System.ComponentModel.DataAnnotations;
using Strategy_Design_Pattern.Business.Models;
using Strategy_Design_Pattern.Business.Strategies;
using Strategy_Design_Pattern.Business.Strategies.Invoice;
using Strategy_Design_Pattern.Business.Strategies.Shipping;

namespace Strategy_Design_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var order = new Order
            {
                ShippingDetails = new ShippingDetails
                {
                    OriginCountry = "Sweden",
                    DestinationCountry = "Sweden"
                },
                // You should build getters for strategies

                //SalesTaxStrategy = GetSalesTaxStreategyFor(origin),
                //InvoiceStrategy = GetInvoiceStrategyFor(invoiceOption),
                //ShippingStrategy = GetShippingStrategyFor(provider)

                //But for now its hardcoded
                SalesTaxStrategy = new SwedenSalesTaxStrategy(),
                InvoiceStrategy = new FileInvoiceStrategy(),
                ShippingStrategy = new SwedishPostalServiceShippingStrategy()
            };

            var destination = order.ShippingDetails.DestinationCountry.ToLowerInvariant();
            switch (destination)
            {
                case "sweden":
                    order.SalesTaxStrategy = new SwedenSalesTaxStrategy();
                    break;
                case "us":
                    order.SalesTaxStrategy = new USAStateSalesTaxStrategy();
                    break;
            }

            order.LineItems.Add(
                new Item("CSHARP_SMORGASBORD",
                    "C# Smorgasbord",
                    100m,
                    ItemType.Food),
                1);
            order.LineItems.Add(
                new Item("CONSULTING",
                    "Building a website",
                    100m,
                    ItemType.Service),
                1);

            order.SelectedPayments.Add(new Payment
            {
                PaymentProvider = PaymentProvider.Invoice
            });

            Console.WriteLine(order.GetTax());

            order.InvoiceStrategy = new FileInvoiceStrategy();
            order.FinalizeOrder();
        }
    }
}
