﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Shipping
{
    class UPSShippingStrategy : IShippingStrategy
    {
        public void Ship(Order order)
        {
            using (var client = new HttpClient())
            {
                //TODO Implement UPS Shipping Integration
                Console.WriteLine("Order is shipped with UPS");
            }
        }
    }
}
