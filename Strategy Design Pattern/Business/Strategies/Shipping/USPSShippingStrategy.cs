﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Shipping
{
    class USPSShippingStrategy : IShippingStrategy
    {
        public void Ship(Order order)
        {
            using (var client = new HttpClient())
            {
                //TODO Implement USPS Shipping Integration
                Console.WriteLine("Order is shipped with United States Postal Service");
            }
        }
    }
}
