﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Shipping
{
    class SwedishPostalServiceShippingStrategy : IShippingStrategy
    {
        public void Ship(Order order)
        {
            using (var client = new HttpClient())
            {
                //TODO Implement PostNord Shipping Integration
                Console.WriteLine("Order is shipped with PostNord");
            }
        }
    }
}
