﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Invoice
{
    class EmailInvoiceStrategy : InvoiceStrategy
    {
        public override void Generate(Order order)
        {
            var body = GenerateTextInvoice(order);

            using SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            try
            {
                NetworkCredential credential = new NetworkCredential("username", "password");
                client.Credentials = credential;

                MailMessage mail = new MailMessage("from", "to")
                {
                    Subject = "We've created an invoice for your order",
                    Body = body
                };
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString(), ex.Message);
            };
        }
    }
}
