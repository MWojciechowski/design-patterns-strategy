﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Invoice
{
    class FileInvoiceStrategy : InvoiceStrategy
    {
        public override void Generate(Order order)
        {
            var NAME = $"invoice_{Guid.NewGuid()}.txt";
            using var stream = new StreamWriter(NAME);
            stream.Write(GenerateTextInvoice(order));
            stream.Flush();
            Console.WriteLine($"File created: {NAME}");
        }
    }
}
