﻿using System;
using System.Collections.Generic;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Invoice
{
    abstract class InvoiceStrategy : IInvoiceStrategy
    {
        public abstract void Generate(Order order);

        public string GenerateTextInvoice(Order order)
        {
            var invoice = $"INVOICE DATE: {DateTime.Now}{Environment.NewLine}";
            invoice += $"ID|NAME|PRICE|QUANTITY{Environment.NewLine}";
            foreach (var item in order.LineItems)
            {
                invoice += $"{item.Key.Id}|{item.Key.Name}|{item.Key.Price}|{item.Value}";
            }
            invoice += Environment.NewLine + Environment.NewLine;
            var tax = order.GetTax();
            var total = order.TotalPrice + tax;
            invoice += $"TAX TOTAL: {tax}{Environment.NewLine}TOTAL: {total}{Environment.NewLine}";
            return invoice;
        }

    }
}
