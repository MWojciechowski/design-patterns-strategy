﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies.Invoice
{
    class PrintOnDemandInvoiceStrategy : IInvoiceStrategy
    {
        public void Generate(Order order)
        {
            using (var client = new HttpClient())
            {
                var content = JsonConvert.SerializeObject(order);
                client.BaseAddress = new Uri("https://pluralsight.com");
                client.PostAsync("/print-on-demand", new StringContent(content));
            }
        }
    }
}
