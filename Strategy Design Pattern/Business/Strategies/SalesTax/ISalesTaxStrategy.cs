﻿using System;
using System.Collections.Generic;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies
{
    interface ISalesTaxStrategy
    {
        public decimal GetTaxFor(Order order);
    }
}
