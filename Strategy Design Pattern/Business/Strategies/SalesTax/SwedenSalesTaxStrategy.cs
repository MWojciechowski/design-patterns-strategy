﻿using System;
using System.Collections.Generic;
using System.Text;
using Strategy_Design_Pattern.Business.Models;

namespace Strategy_Design_Pattern.Business.Strategies
{
    class SwedenSalesTaxStrategy : ISalesTaxStrategy
    {
        public decimal GetTaxFor(Order order)
        {
            decimal total = 0m;
            foreach (var item in order.LineItems)
            {
                switch (item.Key.ItemType)
                {
                    case ItemType.Food:
                        total += item.Key.Price * 0.06m * item.Value;
                        break;
                    case ItemType.Hardware:
                        total += item.Key.Price * 0.08m * item.Value;
                        break;
                    case ItemType.Service:
                    case ItemType.Literature:
                        total += item.Key.Price * 0.25m * item.Value;
                        break;
                }
            }
            return total;
        }
    }
}
